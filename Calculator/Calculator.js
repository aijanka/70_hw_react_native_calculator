import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {connect} from "react-redux";
import {addSymbol, deleteAll, deleteSymbol, viewAnswer} from "../store/actions";

class Calculator extends React.Component {

    symbolsArray = ['+', '-', '/', '*', '%', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];

    // addInputText = i => {
    //     this.setState(prevState => {
    //         return {inputText: prevState.inputText + i.toString()};
    //     })
    // };

    // deleteSymbol = () => {
    //     this.setState(prevState => {
    //         const inputText = prevState.inputText.slice(0, prevState.inputText.length - 1);
    //         return {inputText};
    //     })
    // };

    // deleteAll = () => {
    //     this.setState({inputText: ''});
    // }
    //
    // viewAnswer = () => {
    //     let inputText = '';
    //     try {
    //         inputText = eval(this.state.inputText).toString();
    //     } catch(e) {
    //         inputText = 'Ошибка в вводе!';
    //     } finally {
    //         this.setState({inputText});
    //     }
    // };

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.inputView}>{this.props.inputText}</Text>
                <View style={styles.buttons}>
                    {this.symbolsArray.map(symbol =>
                        (<TouchableOpacity
                            style={styles.button}
                            onPress={() => this.props.addInputText(symbol)}
                            key={symbol}>
                            <Text>{symbol}</Text>
                        </TouchableOpacity>)
                    )}
                    <TouchableOpacity style={styles.button} onPress={this.props.deleteSymbol}><Text>delete</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.props.deleteAll}><Text>delete all</Text></TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={this.props.viewAnswer}><Text>=</Text></TouchableOpacity>
                </View>
            </View>


        );
    }
}

const mapStateToProps = state => ({
    inputText: state.inputText
});

const mapDispatchToProps = dispatch => ({
    addInputText: i => dispatch(addSymbol(i)),
    deleteSymbol: () => dispatch(deleteSymbol()),
    deleteAll: () => dispatch(deleteAll()),
    viewAnswer: () => dispatch(viewAnswer())
});

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);

const styles = StyleSheet.create({
    container: {
        alignSelf: 'center',
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginTop: 40,
        width: 300
    },
    inputView: {
        width: '100%',
        height: 50,
        borderColor: 'black',
        borderWidth: 1
    },
    button: {
        height: 50,
        width: '30%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 5,
        margin: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttons: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    }
});
