import React from 'react';
import Calculator from "./Calculator/Calculator";
import {Provider} from "react-redux";
import {createStore, applyMiddleware} from "redux";
import thunk from 'redux-thunk';
import reducer from "./store/reducer";

export default class App extends React.Component {

    store = createStore(reducer, applyMiddleware(thunk));

    render() {
        return <Provider store={this.store}><Calculator/></Provider>
    }
}

