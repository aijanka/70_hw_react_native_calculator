import * as actionTypes from './actionTypes';

export const addSymbol = i => ({type: actionTypes.ADD_SYMBOL, symbolAdded: i});
export const deleteSymbol = () => ({type: actionTypes.DELETE_SYMBOL});
export const deleteAll =() => ({type: actionTypes.DELETE_ALL});

export const viewAnswer = () => ({type: actionTypes.VIEW_ANSWER});