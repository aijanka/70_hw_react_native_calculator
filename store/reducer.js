import * as actionTypes from './actionTypes';

initialState = {
    inputText: ''
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD_SYMBOL:
            return {inputText: state.inputText + action.symbolAdded.toString()};
        case actionTypes.DELETE_SYMBOL:
            return {inputText: state.inputText.slice(0, state.inputText.length - 1)};
        case actionTypes.DELETE_ALL:
            return {inputText: ''};
        case actionTypes.VIEW_ANSWER:
            let inputText = '';
            try {
                inputText = eval(state.inputText).toString();
                return {inputText};
            } catch(e) {
                return {inputText: 'Ошибка в вводе!'}
            }
        default:
            return state;
    }
};

export default reducer;