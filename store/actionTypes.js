export const ADD_SYMBOL = 'ADD_SYMBOL';
export const DELETE_SYMBOL = 'DELETE_SYMBOL';
export const DELETE_ALL = 'DELETE_ALL';
export const VIEW_ANSWER = 'VIEW_ANSWER';
